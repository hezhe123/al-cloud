package com.ruoyi.gen.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.security.entity.BaseZrdEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 测试用例对象 test_case
 *
 * @author ruoyi
 * @date 2020-12-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TestCase extends BaseZrdEntity {

    /**
     * id主键
     */
    private Integer id;

    /**
     * 删除标识（0正常 1关闭）
     */
    private String delFlag;

    /**
     * 主题
     */
    @Excel(name = "主题" )
    private String theme;

    /**
     * 类型
     */
    @Excel(name = "类型" )
    private String type;

    /**
     * 父级任务
     */
    @Excel(name = "父级任务" )
    private String parentTask;

    /**
     * 经办人
     */
    @Excel(name = "经办人" )
    private String agent;

    /**
     * 优先级
     */
    @Excel(name = "优先级" )
    private String priority;

    /**
     * 状态
     */
    @Excel(name = "状态" )
    private String status;

    /**
     * 解决结果
     */
    @Excel(name = "解决结果" )
    private String solveResults;

    /**
     * 模块
     */
    private String module;

    /**
     * 期望完成时间
     */
    private Date expectedCompletionTime;

    /**
     * 原估时间
     */
    private String originalEstimateTime;

    /**
     * 标签
     */
    private String label;




}
