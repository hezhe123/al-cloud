package com.ruoyi.common.security.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author zrd
 * @version 1.0
 * @date 2020/12/16 17:12
 */
@Configuration
@MapperScan(basePackages = {"com.ruoyi.**.mapper"})
@EnableTransactionManagement
public class MybatisPlusConfig {


    /**
     * 性能分析拦截器，不建议生产使用
     */
//    @Bean
//    public PerformanceInterceptor performanceInterceptor(){
//        return new PerformanceInterceptor();
//    }

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }

    /**
     * 预 SQL 自动填充
     */
    @Bean
    public MyMetaObjectHandler myMetaObjectHandler() {
        return new MyMetaObjectHandler();
    }

    /**
     * pagehelper的分页插件
     *//*
    @Bean
    public PageInterceptor pageInterceptor() {
        return new PageInterceptor();
    }*/




}
